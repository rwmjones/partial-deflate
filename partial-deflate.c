/* Compress a disk image, leaving it partially uncompressed.
 * Copyright (C) 2022 Red Hat Inc., Richard W.M. Jones
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* Usage:
 *  partial-deflate input output.gz [BOUND0 [BOUND1 ...]]
 *
 * The BOUND parameter define byte ranges in the input that you want
 * to be compressed in the output.  Bytes [0..BOUND0-1] are
 * compressed, bytes [BOUND0..BOUND1-1] are copied (uncompressed), the
 * next range is compressed, and so it alternates.
 *
 * Example:
 *  $ guestfish --ro -a fedora-36.img -i part-list /dev/sda
 *  [0] = {
 *    part_num: 1
 *    part_start: 1048576
 *    part_end: 2097151
 *    part_size: 1048576
 *  }
 *  [1] = {
 *    part_num: 2
 *    part_start: 2097152
 *    part_end: 1075838975
 *    part_size: 1073741824
 *  }
 *  [2] = {
 *    part_num: 3
 *    part_start: 1075838976      # start of /dev/sda3
 *    part_end: 6441402367        # last byte of /dev/sda3
 *    part_size: 5365563392
 *  }
 *
 * We only want to compress partition #2 (/dev/sda3), so:
 *  partial-deflate disk disk.gz 0 1075838976 6441402368
 *
 * Note that while you can update the uncompressed parts of the
 * compressed file in-place, the CRC32 at the end of the gzip file
 * will be wrong, resulting in a CRC error when you uncompress the
 * file with normal tools.  If you know what you removed and what you
 * added, you can update the CRC without recalculating it for the
 * whole file - exercise left to reader.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <error.h>
#include <assert.h>

#include <zlib.h>

static char inbuf[256*1024];
static char outbuf[256*1024];

static void xwrite (const char *msg, int fd, const char *buf, size_t len);

int
main (int argc, char *argv[])
{
  z_stream c_stream = { 0 };
  gz_header head = { 0 };
  int infd, outfd, err;
  bool compressing = true;
  int next_optind = 3;
  int64_t next;

  if (argc < 3)
    error (EXIT_FAILURE, 0,
           "expected: %s input output [boundaries...]", argv[0]);

  infd = open (argv[1], O_RDONLY);
  if (infd == -1)
    error (EXIT_FAILURE, errno, "open: %s", argv[1]);

  outfd = open (argv[2], O_WRONLY|O_TRUNC|O_CREAT, 0644);
  if (outfd == -1)
    error (EXIT_FAILURE, errno, "open: %s", argv[2]);

  /* Set up the first boundary. */
  if (argv[next_optind] != NULL) {
    if (sscanf (argv[next_optind], "%" SCNi64, &next) != 1 ||
        next < 0)
      error (EXIT_FAILURE, 0,
             "could not parse boundary: %s", argv[next_optind]);
    next_optind++;
  }
  else {
    next = -1;
  }

  /* Open the deflate stream. */
  c_stream.zalloc = Z_NULL;
  c_stream.zfree = Z_NULL;
  c_stream.opaque = NULL;

  err = deflateInit2 (&c_stream, Z_BEST_COMPRESSION,
                      Z_DEFLATED, 15+16, 8, Z_DEFAULT_STRATEGY);
  if (err != Z_OK)
    error (EXIT_FAILURE, 0, "zlib: deflateInit2: %d", err);

  head.time = time (NULL);
  head.name = (Bytef *) argv[1];
  err = deflateSetHeader (&c_stream, &head);
  if (err != Z_OK)
    error (EXIT_FAILURE, 0, "zlib: deflateSetHeader: %d", err);

  /* Set up output buffer. */
  c_stream.next_out = (Bytef *) outbuf;
  c_stream.avail_out = sizeof outbuf;

  /* Set input buffer empty.  Below deflate will return Z_BUF_ERROR
   * and we'll reload this.
   */
  c_stream.next_in = (Bytef *) inbuf;
  c_stream.avail_in = 0;

  for (;;) {
    /* Have we reached the next boundary on the input? */
    if (c_stream.total_in == next) {
      assert (c_stream.avail_in == 0);
#if 0
      fprintf (stderr,
               "@ %" PRIi64 " flipping to %s ...\n",
               next, compressing ? "no compression" : "best compression");
#endif
      err = deflateParams (&c_stream,
                           /* We're about to switch compressing mode,
                            * hence this is inverted.
                            */
                           compressing ? Z_NO_COMPRESSION : Z_BEST_COMPRESSION,
                           Z_DEFAULT_STRATEGY);
      if (err == Z_OK) {
        compressing = !compressing;
        if (argv[next_optind] != NULL) {
          if (sscanf (argv[next_optind], "%" SCNi64, &next) != 1 ||
              next < 0)
            error (EXIT_FAILURE, 0,
                   "could not parse boundary: %s", argv[next_optind]);
          next_optind++;
        }
        else {
          next = -1;
        }
      }
      else if (err != Z_BUF_ERROR)
        error (EXIT_FAILURE, 0, "zlib: deflateParams: %d", err);
    }
    /* Otherwise normal deflate. */
    else {
      err = deflate (&c_stream, Z_NO_FLUSH);
      if (err != Z_OK && err != Z_BUF_ERROR)
        error (EXIT_FAILURE, 0, "zlib: deflate: %d", err);
    }

    /* We've filled the output buffer, write it to the file. */
    if (c_stream.avail_out == 0) {
      xwrite (argv[2], outfd, outbuf, sizeof outbuf);
      c_stream.next_out = (Bytef *) outbuf;
      c_stream.avail_out = sizeof outbuf;
      continue;
    }

    /* While there is more input, load input buffer. */
    if (c_stream.avail_in == 0 && c_stream.total_in != next) {
      size_t len = sizeof inbuf;
      int64_t limit;

      if (next != -1) {
        limit = next - c_stream.total_in;
        if (limit < len)
          len = limit;
      }
      assert (len > 0);

      err = read (infd, inbuf, len);
      if (err == -1)
        error (EXIT_FAILURE, errno, "read: %s", argv[1]);
      if (err == 0)
        goto finish;
      c_stream.next_in = (Bytef *) inbuf;
      c_stream.avail_in = err;
    }
  }

 finish:
  for (;;) {
    err = deflate (&c_stream, Z_FINISH);
    if (err == Z_STREAM_END)
      break;
    if (err != Z_OK && err != Z_BUF_ERROR)
      error (EXIT_FAILURE, 0, "zlib: deflate: %d", err);

    if (c_stream.avail_out == 0) {
      /* We've filled the output buffer, write it to the file. */
      xwrite (argv[2], outfd, outbuf, sizeof outbuf);
      c_stream.next_out = (Bytef *) outbuf;
      c_stream.avail_out = sizeof outbuf;
    }
  }

  /* Write anything left in the output buffer. */
  xwrite (argv[2], outfd, outbuf, (char *) c_stream.next_out - outbuf);

  err = deflateEnd (&c_stream);
  if (err != Z_OK)
    error (EXIT_FAILURE, 0, "zlib: deflateEnd: %d", err);

  if (close (infd) == -1)
    error (EXIT_FAILURE, errno, "close: %s", argv[1]);
  if (close (outfd) == -1)
    error (EXIT_FAILURE, errno, "close: %s", argv[2]);

  exit (EXIT_SUCCESS);
}

/* Write len bytes to fd, and fail if there is an error. */
static void
xwrite (const char *msg, int fd, const char *buf, size_t len)
{
  int r;

  while (len > 0) {
    r = write (fd, buf, len);
    if (r == -1)
      error (EXIT_FAILURE, errno, "%s: write", msg);
    buf += r;
    len -= r;
  }
}
