CC	:= gcc
#CFLAGS	:= -Wall -O2 -g
CFLAGS	:= -Wall -O0 -g
LIBS	:= -lz

all: partial-deflate

partial-deflate: partial-deflate.c
	$(CC) $(CFLAGS) $< -o $@ $(LIBS)

clean:
	rm -f partial-deflate *~
